<?php declare(strict_types=1);

namespace Parchex\Third\Behat\Context;

use Assert\Assertion;
use Assert\AssertionFailedException as AssertionFailure;
use Behat\Gherkin\Node\PyStringNode;
use Imbo\BehatApiExtension\ArrayContainsComparator;
use Imbo\BehatApiExtension\Context\ApiContext as AC;
use Imbo\BehatApiExtension\Exception\ArrayContainsComparatorException;
use Imbo\BehatApiExtension\Exception\AssertionFailedException;
use JsonException;

class ApiContext extends AC
{
    public const UUID4_PATTERN =
        "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}";

    public function setArrayContainsComparator(ArrayContainsComparator $comparator)
    {
        $comparator->addFunction(
            'uuid4',
            static function ($identifier): void {
                $identifier = (string) $identifier;

                if (preg_match('/' . self::UUID4_PATTERN . '/', $identifier) !== 1) {
                    throw new \InvalidArgumentException(
                        sprintf(
                            'Subject "%s" is not a valid uuid4 identifier.',
                            $identifier
                        )
                    );
                }
            }
        );

        return parent::setArrayContainsComparator($comparator);
    }

    /**
     * Assert that the response body contains links structure with correct href
     *
     * @param PyStringNode $contains
     *
     * @return void
     *
     * @throws AssertionFailedException
     * @throws JsonException
     *
     * @Then the response body contains links:
     */
    public function theResponseBodyContainsLinks(PyStringNode $contains): void
    {
        $this->requireResponse();

        $expected = $this->parseComparatorLabels(
            $this->jsonDecode(
                str_replace(
                    "{base_uri}",
                    $this->client->getConfig('base_uri'),
                    $contains->getRaw()
                )
            )
        );

        // Get the decoded response body and make sure it's decoded to an array
        $body = $this->getResponseBodyJsonDecoded();
        try {
            // Compare the arrays, on error this will throw an exception
            Assertion::true($this->arrayContainsComparator->compare($expected, $body));
        } catch (AssertionFailure $exc) {
            throw new AssertionFailedException(
                'Comparator did not return in a correct manner. Marking assertion as failed.'
            );
        }
    }

    /**
     * Assert that the response body contains all keys / values in the parameter
     *
     * @Then the response body not contains JSON:
     *
     * @throws AssertionFailedException
     * @throws JsonException
     */
    public function assertResponseBodyNotContainsJson(PyStringNode $contains): void
    {
        $this->requireResponse();
        // Get the decoded response body and make sure it's decoded to an array
        $body = $this->getResponseBodyJsonDecoded();
        // Decode the parameter to the step as an array and make sure it's valid JSON
        $expected = $this->jsonDecode($contains->getRaw());

        try {
            // Compare the arrays, on error this will throw an exception
            $this->arrayContainsComparator->compare($expected, $body);
        } catch (ArrayContainsComparatorException $exc) {
            return;
        }
        throw new ArrayContainsComparatorException(
            'The Json exists in Response Body.',
            0,
            null,
            $expected,
            $body
        );
    }

    /**
     * @throws JsonException
     */
    private function getResponseBodyJsonDecoded(): array
    {
        return json_decode(
            json_encode($this->getResponseBody(), JSON_THROW_ON_ERROR),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
    }

    private function parseComparatorLabels(array $contains): array
    {
        foreach ($contains as $key => $links) {
            $contains[$key] = array_map(
                static function ($link) {
                    $href = $link['href'] ?? $link;
                    if (strpos($href, '{uuid4}') === false) {
                        return $link;
                    }

                    $href = sprintf(
                        "@regExp(/%s/)",
                        str_replace(
                            ["/", ".", "?", "{uuid4}"],
                            ['\/', '\.', '\?', self::UUID4_PATTERN],
                            $href
                        )
                    );

                    if (is_array($link)) {
                        $link['href'] = $href;
                    } else {
                        $link = $href;
                    }

                    return $link;
                },
                $links
            );
        }

        return $contains;
    }
}

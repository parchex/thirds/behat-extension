<?php declare(strict_types=1);

namespace Parchex\Third\Behat\ServiceContainer;

use Behat\Behat\HelperContainer\ServiceContainer\HelperContainerExtension;
use Behat\Testwork\ServiceContainer\Extension;
use Behat\Testwork\ServiceContainer\ExtensionManager;
use Slim\Container;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SlimContainerExtension implements Extension
{
    /**
     * {@inheritdoc}
     */
    public function getConfigKey()
    {
        return 'slim_container';
    }

    public function configure(ArrayNodeDefinition $builder): void
    {
        $builder
            ->children()
            ->scalarNode('config_file')
            ->defaultValue('config/settings.php')
            ->end()
            ->scalarNode('dependencies_file')
            ->defaultValue('config/dependencies.php')
            ->end()
            ->scalarnode('singleton')
            ->defaultValue('true')
            ->end();
    }

    public function load(ContainerBuilder $container, array $config): void
    {
        $buildConfig = [
            SlimContainerFactory::PARAM_NAME_PATH_BASE => $container->getParameter('paths.base'),
            SlimContainerFactory::PARAM_NAME_CONFIG_FILE => $config['config_file'],
            SlimContainerFactory::PARAM_NAME_DEPENDENCE_FILE => $config['dependencies_file'],
            SlimContainerFactory::PARAM_NAME_SINGLETON => filter_var(
                $config['singleton'],
                FILTER_VALIDATE_BOOLEAN
            ),
        ];

        $container->register('slim.container', Container::class)
            ->addArgument($buildConfig)
            ->setShared(false)
            ->setPublic(true)
            ->addTag(
                HelperContainerExtension::HELPER_CONTAINER_TAG,
                ['priority' => 0]
            )
            ->setFactory(SlimContainerFactory::class . '::buildContainer');
    }

    public function process(ContainerBuilder $container): void
    {
        // Nothing to do
    }

    public function initialize(ExtensionManager $extensionManager): void
    {
        // Nothing to do
    }
}

<?php declare(strict_types=1);

namespace Parchex\Third\Behat\ServiceContainer;

use Slim\Container;

class SlimContainerFactory
{
    public const PARAM_NAME_PATH_BASE = 'path_base';
    public const PARAM_NAME_CONFIG_FILE = 'config_file';
    public const PARAM_NAME_DEPENDENCE_FILE = 'dependencies_file';
    public const PARAM_NAME_SINGLETON = 'singleton';
    /**
     * @var Container
     */
    private static $container;

    public static function buildContainer(array $config): Container
    {
        if (isset($config[self::PARAM_NAME_SINGLETON], self::$container) &&
            $config[self::PARAM_NAME_SINGLETON] === true) {
            return self::$container;
        }

        return self::createContainer($config);
    }

    private static function createContainer(array $config): Container
    {
        $settings = require $config[self::PARAM_NAME_PATH_BASE] . DIRECTORY_SEPARATOR .
            $config[self::PARAM_NAME_CONFIG_FILE];

        $container = new Container($settings);

        require $config[self::PARAM_NAME_PATH_BASE] . DIRECTORY_SEPARATOR .
            $config[self::PARAM_NAME_DEPENDENCE_FILE];

        if (isset($config[self::PARAM_NAME_SINGLETON]) &&
            $config[self::PARAM_NAME_SINGLETON] === true) {
            self::$container = $container;
        }

        return $container;
    }
}

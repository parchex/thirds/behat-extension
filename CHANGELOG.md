# Changelog

## v0.1.4 - 2021-10-14

* New function to check UUID literals
* New literal assert to check links


## v0.1.3 - 2021-10-14

* Update tools and versions

## v0.1.0 - 2020-11-17

* Initial release
    - **API Context** from Behat
    - **Slim Container** to use in contexts

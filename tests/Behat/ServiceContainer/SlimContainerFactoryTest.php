<?php

namespace Tests\Parchex\Third\Behat\ServiceContainer;

use DateTimeImmutable;
use Parchex\Third\Behat\ServiceContainer\SlimContainerFactory;
use PHPUnit\Framework\TestCase;
use Slim\Container;

/**
 * @testdox Create container for Slim framework
 * @covers  \Parchex\Third\Behat\ServiceContainer\SlimContainerFactory
 */
class SlimContainerFactoryTest extends TestCase
{
    protected $config;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->config = [
            SlimContainerFactory::PARAM_NAME_PATH_BASE => __DIR__ . '/../../.',
            SlimContainerFactory::PARAM_NAME_CONFIG_FILE => 'config/settings.php',
            SlimContainerFactory::PARAM_NAME_DEPENDENCE_FILE => 'config/dependencies.php',
        ];
    }

    /**
     * @test
     */
    public function it_creates_a_container()
    {
        $container = SlimContainerFactory::buildContainer($this->config);

        self::assertInstanceOf(Container::class, $container);
    }

    /**
     * @test
     */
    public function it_creates_container_with_configured_dependencies()
    {
        $container = SlimContainerFactory::buildContainer($this->config);

        $service = $container['service.date'];
        $otherService = $container['service.date'];

        self::assertInstanceOf(DateTimeImmutable::class, $service);
        self::assertSame($service, $otherService);
    }

    /**
     * @test
     */
    public function it_creates_a_singleton_container_instance()
    {
        $this->config[SlimContainerFactory::PARAM_NAME_SINGLETON] = true;

        $container = SlimContainerFactory::buildContainer($this->config);
        $otherContainer = SlimContainerFactory::buildContainer($this->config);

        self::assertInstanceOf(Container::class, $container);
        self::assertSame($container, $otherContainer);
    }

    /**
     * @test
     */
    public function it_creates_different_container_instances()
    {
        $this->config[SlimContainerFactory::PARAM_NAME_SINGLETON] = false;

        $container = SlimContainerFactory::buildContainer($this->config);
        $otherContainer = SlimContainerFactory::buildContainer($this->config);

        self::assertInstanceOf(Container::class, $container);
        self::assertInstanceOf(Container::class, $otherContainer);
        self::assertEquals($container, $otherContainer);
    }
}

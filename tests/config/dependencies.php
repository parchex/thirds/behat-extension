<?php

use Slim\Container;

// DIC configuration
/** @var Container $container */
$container = $container ?? $app->getContainer();

// Definitions dependencies
$container['service.date'] = static function ($c) {
    return new DateTimeImmutable();
};
